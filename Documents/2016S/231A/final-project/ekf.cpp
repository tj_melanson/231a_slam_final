#include "ekf.hpp"
#include <Eigen/LU>
#include <iostream>

using namespace Eigen;


EKF::EKF(MatrixXd P0, MatrixXd G0, MatrixXd R_init){
	Pk = P0;
	Gk = G0;
	R  = R_init;

	n = P0.rows();
	m = R.rows();
}

//In case there is no value for initial Pk, Gk
EKF::EKF(int n_arg, int m_arg){
	n= n_arg;
	m= m_arg;
	Pk = MatrixXd::Identity(n,n);
	Gk = MatrixXd::Identity(n,m);
	R  = MatrixXd::Identity(m,m);
}

//NOTE: this is for obesrvations that change with time 
//Should not use for cases like SLAM for stationary object
void EKF::timeUpdate(VectorXd f_x, MatrixXd Fk, MatrixXd Qk, VectorXd &xk){
	xk = f_x;
	Pk = Fk*Pk*Fk.transpose() + Qk;
}

void EKF::observationUpdate(VectorXd h_x, MatrixXd Hk, VectorXd zk, VectorXd &xk){
	
	//Update gain Gl with Hk, R
	Gk = Pk*Hk.transpose()*PartialPivLU<MatrixXd>(Hk*Pk*Hk.transpose() + R).inverse(); 

	// std::cout << "Gk: \n" << Gk << std::endl;
	// std::cout << "Pk: \n" << Pk << std::endl;
	
	//Compute xk from observation, gain
	xk = xk + Gk*(zk - h_x);

	//Compute new covariance Pk
	Pk = (MatrixXd::Identity(n, n) - Gk*Hk)*Pk;

}
