#include <stdio.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;

void readme();

/** @function main */
int main( int argc, char** argv )
{

  char filename1[FILENAME_MAX];
  char filename2[FILENAME_MAX];

  int min_image = 1;
  int max_image = 701;
  int step      = 5;
  int image_id  = min_image;
 
  while (image_id < max_image)
     {
     
     snprintf (filename1, FILENAME_MAX, "img%05d.png", image_id);
     image_id += step;
     snprintf (filename2, FILENAME_MAX, "img%05d.png", image_id);

     Mat img_1 = imread(filename1, CV_LOAD_IMAGE_GRAYSCALE );
     Mat img_2 = imread(filename2, CV_LOAD_IMAGE_GRAYSCALE );



     if( !img_1.data || !img_2.data )
        {std::cout<< " --(!) Error reading images " << std::endl; return -1; }

     //-- Step 1: Detect the keypoints using SURF Detector
     int minHessian = 400;

     SurfFeatureDetector detector( minHessian );

     std::vector<KeyPoint> keypoints_1, keypoints_2;
   
     detector.detect( img_1, keypoints_1 );
     detector.detect( img_2, keypoints_2 );

     //-- Step 2: Calculate descriptors (feature vectors)
     SurfDescriptorExtractor extractor;

     Mat descriptors_1, descriptors_2;

     extractor.compute( img_1, keypoints_1, descriptors_1 );
     extractor.compute( img_2, keypoints_2, descriptors_2 );

     //-- Step 3: Matching descriptor vectors using FLANN matcher
     FlannBasedMatcher matcher;
     std::vector< DMatch > matches;
     matcher.match( descriptors_1, descriptors_2, matches );

     double max_dist = 0; double min_dist = 100;

  //-- Quick calculation of max and min distances between keypoints
  for( int i = 0; i < descriptors_1.rows; i++ )
  { double dist = matches[i].distance;
    if( dist < min_dist ) min_dist = dist;
    if( dist > max_dist ) max_dist = dist;
  }

  printf("-- Max dist : %f \n", max_dist );
  printf("-- Min dist : %f \n", min_dist );

  //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
  std::vector< DMatch > good_matches;

  for( int i = 0; i < descriptors_1.rows; i++ )
  { if( matches[i].distance < 3*min_dist )
     { good_matches.push_back( matches[i]); }
  }

   Mat img_matches;
  
   //------------------
   //This draws the two images side-by-side with connecting lines
   //------------------
   /*
   drawMatches( img_1, keypoints_1, img_2, keypoints_2,
                good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
   */

  
   //------------------
   //This draws two images side-by-side, manually draw lines
   //------------------
   Size size    = img_1.size();
   Size sz      = Size(2 * size.width, size.height);
   img_matches  = Mat::zeros(sz, CV_8UC1);   
   
   // Draw original image
   Mat roi1 = Mat(img_matches, Rect(0, 0, size.width, size.height));
   img_1.copyTo(roi1);
  
   //Draw matching image 
    Mat roi2 = Mat(img_matches, Rect(size.width, 0, size.width, size.height));
   img_2.copyTo(roi2);

  
   // Draw line on composite image between origin and new position
   // Save point pairs
   char filename_pts[FILENAME_MAX];
   snprintf (filename_pts, FILENAME_MAX, "pts_%05d_%05d.csv", image_id - step, image_id);
   FILE * pFile;
   pFile = fopen (filename_pts , "w");
   if (pFile == NULL) {perror ("Error opening file");return -1;}
   for( int i = 0; i < good_matches.size(); i++ )
      {
      Point2f pt1  = keypoints_1[ good_matches[i].queryIdx ].pt ;
      Point2f pt2  = keypoints_2[ good_matches[i].trainIdx ].pt ;
      line(img_matches, pt1, pt2, Scalar(0, 255, 255));
      fprintf(pFile, "%d,%.1f,%.1f:%.1f,%.1f\n", i,pt1.x,pt1.y,pt2.x,pt2.y);
      }

   fclose (pFile);



/*

  //-- Localize the object
  std::vector<Point2f> pts1;
  std::vector<Point2f> pts2;

  for( int i = 0; i < good_matches.size(); i++ )
  {
    //-- Get the keypoints from the good matches
    pts1.push_back( keypoints_1[ good_matches[i].queryIdx ].pt );
    pts2.push_back( keypoints_2[ good_matches[i].trainIdx ].pt );
  }

  Mat H = findHomography( pt1, pt2, CV_RANSAC );

  //-- Get the corners from the image_1 ( the object to be "detected" )
  std::vector<Point2f> obj_corners(4);
  obj_corners[0] = cvPoint(          0, 0);
  obj_corners[1] = cvPoint( img_1.cols, 0 );
  obj_corners[2] = cvPoint( img_1.cols, img_1.rows ); 
  obj_corners[3] = cvPoint(          0, img_1.rows );
  std::vector<Point2f> scene_corners(4);

  perspectiveTransform( obj_corners, scene_corners, H);

  //-- Draw lines between the corners (the mapped object in the scene - image_2 )
  line( img_matches, scene_corners[0] + Point2f( img_1.cols, 0), scene_corners[1] + Point2f( img_1.cols, 0), Scalar( 0, 255, 0), 4 );
  line( img_matches, scene_corners[1] + Point2f( img_1.cols, 0), scene_corners[2] + Point2f( img_1.cols, 0), Scalar( 0, 255, 0), 4 );
  line( img_matches, scene_corners[2] + Point2f( img_1.cols, 0), scene_corners[3] + Point2f( img_1.cols, 0), Scalar( 0, 255, 0), 4 );
  line( img_matches, scene_corners[3] + Point2f( img_1.cols, 0), scene_corners[0] + Point2f( img_1.cols, 0), Scalar( 0, 255, 0), 4 );
*/

  //-- Show detected matches
  imshow( "Image Movement", img_matches );
 
  int c = waitKey(500);
  if (c == '\x1b') break;
  }


  return 0;
  }

  /** @function readme */
  void readme()
  { std::cout << " Usage: ./SURF_descriptor <img1> <img2>" << std::endl; }
