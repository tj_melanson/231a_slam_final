#include <Eigen/Dense>

using namespace Eigen;

class EKF {
	public:
	
	// n: length of vector xk
	// m: length of estimate zk
	int n, m;

	MatrixXd Pk, R; //Initial covariance, k-covariance, variance
	MatrixXd Gk; //Iterative gain value for xk

	EKF(MatrixXd P0, MatrixXd G0, MatrixXd R);
	EKF(int n_arg, int m_arg);

	//xk , xkp1 = x(k+1)
	void timeUpdate(VectorXd f_x, MatrixXd Fk, MatrixXd Qk, VectorXd &xk); //Control covariance 
	void observationUpdate(VectorXd h_x, MatrixXd Hk, VectorXd zk, VectorXd &xk); //observation estimate, observation Jacobian, actual observation
};