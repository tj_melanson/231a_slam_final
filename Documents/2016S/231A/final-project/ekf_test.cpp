#include "ekf.hpp"
#include <Eigen/Dense>
#include <random>
#include <iostream>

VectorXd h(VectorXd x){
	VectorXd v(3);
	v << 1,1,1;
	return x;
}

VectorXd f(VectorXd x){
	return 1.5*x;
}

int main(int argc, char **argv){

	int n=3, m=3;
	MatrixXd Qk = MatrixXd::Zero(n,n);
	
	//Observation and Jacobian
	VectorXd zk(m);
	zk << 0,0,0;
	MatrixXd Hk = MatrixXd::Identity(m,n);

	EKF filter(n,m);
	VectorXd xk(n);
	xk << 1,1,1;
	MatrixXd Fk = MatrixXd::Identity(n,n);

	

	

	for (int i=0; i<20; i++){
		filter.timeUpdate(f(xk), Fk, Qk, xk);
		filter.observationUpdate(h(xk), Hk, zk, xk);
		std::cout << "xk: " << xk.transpose() << std::endl; 
	}
	return 0;

}