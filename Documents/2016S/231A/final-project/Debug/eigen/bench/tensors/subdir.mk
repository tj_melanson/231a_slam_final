################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../eigen/bench/tensors/benchmark_main.cc \
../eigen/bench/tensors/contraction_benchmarks_cpu.cc \
../eigen/bench/tensors/tensor_benchmarks_cpu.cc 

OBJS += \
./eigen/bench/tensors/benchmark_main.o \
./eigen/bench/tensors/contraction_benchmarks_cpu.o \
./eigen/bench/tensors/tensor_benchmarks_cpu.o 

CC_DEPS += \
./eigen/bench/tensors/benchmark_main.d \
./eigen/bench/tensors/contraction_benchmarks_cpu.d \
./eigen/bench/tensors/tensor_benchmarks_cpu.d 


# Each subdirectory must supply rules for building sources it contributes
eigen/bench/tensors/%.o: ../eigen/bench/tensors/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


