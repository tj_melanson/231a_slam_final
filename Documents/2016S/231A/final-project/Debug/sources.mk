################################################################################
# Automatically-generated file. Do not edit!
################################################################################

O_SRCS := 
CPP_SRCS := 
C_UPPER_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
OBJ_SRCS := 
ASM_SRCS := 
CXX_SRCS := 
C++_SRCS := 
CC_SRCS := 
C++_DEPS := 
OBJS := 
C_DEPS := 
CC_DEPS := 
CPP_DEPS := 
EXECUTABLES := 
CXX_DEPS := 
C_UPPER_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
. \
eigen/unsupported/test \
eigen/unsupported/doc/examples \
eigen/unsupported/bench \
eigen/test \
eigen/scripts \
eigen/lapack \
eigen/failtest \
eigen/doc \
eigen/doc/special_examples \
eigen/doc/snippets \
eigen/doc/examples \
eigen/demos/opengl \
eigen/demos/mix_eigen_and_c \
eigen/demos/mandelbrot \
eigen/blas \
eigen/blas/f2c \
eigen/bench \
eigen/bench/tensors \
eigen/bench/spbench \
eigen/bench/perf_monitoring/gemm \
eigen/bench/btl/libs/ublas \
eigen/bench/btl/libs/tvmet \
eigen/bench/btl/libs/tensors \
eigen/bench/btl/libs/mtl4 \
eigen/bench/btl/libs/gmm \
eigen/bench/btl/libs/eigen3 \
eigen/bench/btl/libs/eigen2 \
eigen/bench/btl/libs/blitz \
eigen/bench/btl/libs/blaze \
eigen/bench/btl/libs/STL \
eigen/bench/btl/libs/BLAS \
eigen/bench/btl/data \
build/CMakeFiles \
build/CMakeFiles/3.2.3/CompilerIdCXX \
build/CMakeFiles/3.2.3/CompilerIdC \

