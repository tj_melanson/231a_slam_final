
#include <Eigen/Dense>
#include <Eigen/LU>

using namespace Eigen;

MatrixXd hJacobian(Matrix3d cam, Vector3d position, Vector3d projected_point){
	MatrixXd Jh(2,3);
	x = position(0) + projected_point(0);
	y = position(1) + projected_point(1);
	z = position(2) + projected_point(2);
	Jh << 	cam(0,0)/z, 	0, 				-1*x*cam(0,0)/(z^2),
			0, 				cam(1,1)/z, 	-1*y*cam(1,1)/(z^2);
}

Matrix3d fJacobian(Matrix3d cam, Vector3d position, Vector3d projected_point){
	return Matrix3d::Identity();
}