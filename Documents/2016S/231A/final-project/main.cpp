#include "main.h"



void readPoints(std::string file, MatrixXd &points) {
	std::ifstream iFile;
	iFile.open(file.c_str());

	if (!iFile) return;

    std::stringstream buffer;
    buffer << iFile.rdbuf();

	for (int i=0; i<points.size(); i++){
		buffer >> points(i/points.cols(), i%points.cols());
	}

	iFile.close();
}

int main(int argc, char **argv){

	if (argc < 5){
		std::cout << "Need 4 arguments: world image camera n_points" << std::endl;
		return 1;
	}

	int n_points = atoi(argv[4]);

	MatrixXd X(n_points, 4);
	readPoints(argv[1], X);

	std::cout << "Read world coordinate matrix: \n" << X << std::endl;


	MatrixXd xp(n_points, 2);
	readPoints(argv[2], xp);
	
	std::cout << "Read image coordinates: \n" << xp << std::endl;

	MatrixXd K(3, 3);
	readPoints(argv[3], K);

	std::cout << "Read intrinsic params: \n" << K << std::endl;

	// K [ R t ] X = xp
	//
	// Find T = [R t]
	// [ K11*X^T K12*X^T (K13 - xp1*K33)X^T ] resize(T', 9, 1) = [0] 
	// [    0    K22*X^T (K23 - xp2*K33)X^T ]					 [0]
	MatrixXd Xk(2*n_points, 12); //The matrix used to solve for T
	for (int i=0; i<n_points; i++){
		Xk.block(i*2, 0, 2, 12) << 	K(0,0)*X.row(i), 		K(0,1)*X.row(i), 	(K(0,2) - xp(i,0)*K(2,2))*X.row(i),
									MatrixXd::Zero(1,4),  	K(1,1)*X.row(i), 	(K(1,2) - xp(i,1)*K(2,2))*X.row(i);
	}

	std::cout << "Xk: \n" << Xk << std::endl;

	MatrixXd A = Xk.adjoint() * Xk;

	VectorXd t_vec = SelfAdjointEigenSolver<MatrixXd>(A).eigenvectors().col(0); 
	//VectorXd t_vec = Xk.jacobiSvd(ComputeThinU|ComputeThinV).computeV().col(11);
 

	MatrixXd T(3,4);
	T << t_vec.block(0,0,4,1).transpose(), t_vec.block(4,0,4,1).transpose(), t_vec.block(8,0,4,1).transpose();

	//x = m.colPivHouseholderQr().solve(b);
	// MatrixXf m(2,3);
	// m << 2 , 3 ,5 , -4 , 2, 3;
	// MatrixXf A = m.adjoint() * m;
	// VectorXf x = SelfAdjointEigenSolver<Matrix3f>(A).eigenvectors().col(0);



	// x = m.jacobiSvd(ComputeThinU|ComputeThinV).solve(b);
	// m.transposeInPlace();
	// m.resize(4,1);

	std::cout << "Solution x: \n" << t_vec << std::endl;
	std::cout << "Transformation: \n" << T << std::endl;

	MatrixXd reproj = K*T*X.transpose();
	for (int i=0; i< reproj.rows(); i++)
		reproj.row(i) = reproj.row(i).array() / reproj.bottomRows(1).array();

	std::cout << "Reprojected image: \n" << reproj << std::endl;
	// std::cout << "V matrix: \n" << t_vec << std::endl;
	// std::cout << "AV: \n" << Xk*t_vec << std::endl;
}
