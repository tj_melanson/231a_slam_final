#include <Eigen/Dense>
#include <Eigen/Jacobi>
#include <Eigen/LU>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "jacobian.hpp"
#include "ekf.hpp"

using namespace Eigen;

#define PIXELS_PER_MM 3.779

void formLine(Vector3d pt1, Vector3d pt2,  Vector3d &result){
	result(0) = pt1(1) - pt2(1);
	result(1) = pt2(0) - pt1(0);
	result(2) = pt1(0)*pt2(1) - pt2(0)*pt1(1);
}

void triangulate(Matrix3d K, Matrix3d R, Vector3d t, Vector3d p1_h, Vector3d p2_h, Vector3d &output){

	Vector3d l1;
	Vector3d l11 = Inverse<Matrix3d>(K)*p1_h;
	Vector3d l12 = Vector3d::Zero();
	formLine(l11, l12, l1);
	std::cout << "l1: " << l1.transpose() << std::endl;

	Vector3d l2;
	Vector3d l21 = R.transpose()*(Inverse<Matrix3d>(K)*p2_h - t);
	Vector3d l22 = R.transpose()*(t);
	formLine(l21, l22, l2);
	std::cout << "l2: " << l2.transpose() << std::endl;
	output = l1.cross(l2);

}

void readPoints(std::string file, MatrixXd &points) {
	std::ifstream iFile;
	iFile.open(file.c_str());

	if (!iFile) return;

    std::stringstream buffer;
    buffer << iFile.rdbuf();

	for (int i=0; i<points.size(); i++){
		buffer >> points(i/points.cols(), i%points.cols());
	}

	iFile.close();
}


int main(int argc, char **argv){

	if (argc < 5){
		std::cout << "Need 4 arguments: image1 image2 camera n_points" << std::endl;
		return 1;
	}

	int n_points = atoi(argv[4]);


	MatrixXd xp1(n_points, 2);
	readPoints(argv[1], xp1);
	xp1 = xp1 * PIXELS_PER_MM;

	std::cout << "Read image coordinates: \n" << xp1 << std::endl;

	MatrixXd xp2(n_points, 2);
	readPoints(argv[2], xp2);
	xp2 = xp2 * PIXELS_PER_MM;

	std::cout << "Read image coordinates: \n" << xp2 << std::endl;


	MatrixXd K(3,3);
	readPoints(argv[3], K);

	std::cout << "Read intrinsic params: \n" << K << std::endl;

	Matrix3d R = Matrix3d::Identity();

	Vector3d t;
	t << -305, 0 , 0;

	std::cout << "Created R and t" << std::endl;

	//EKF filter for translation: zk of dimension 2 and xk of dimension 3
	EKF filter(3, 2);

	MatrixXd Jh(2,3);
	Matrix3d Jf;

	MatrixXd points(3, n_points);


	for (int i=0; i<n_points; i++){	
		Vector3d p1, p2, output;
		p1 << xp1(i,0), xp1(i,1), 1;
		p2 << xp2(i,0), xp1(i,1), 1;

		triangulate((Matrix<double, 3, 3>) K, R, t, p1, p2, output);

		Jh = hJacobian(K, t, output); //Relates how subject the image points are to change
		Jf = fJacobian(K, t, output); //For now, just the identity


		filter.timeUpdate()

		points.col(i) = output.transpose();
	}


	std::cout << "Output: \n" << points << std::endl;
	return 0;

}