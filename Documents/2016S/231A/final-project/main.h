/*
 * main.h
 *
 *  Created on: May 31, 2016
 *      Author: tjmelanson
 */

#ifndef MAIN_H_
#define MAIN_H_


#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Dense>
#include <Eigen/Jacobi>
#include <vector>
#include <string>
using namespace Eigen;


#endif /* MAIN_H_ */
