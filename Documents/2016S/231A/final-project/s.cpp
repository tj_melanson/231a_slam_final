#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>


using namespace cv;

int main(int argc, const char* argv[])
{
    const Mat input = imread("input.jpg", 0); //Load as grayscale

    initModule_nonfree();
    Ptr<FeatureDetector> detector = FeatureDetector::create("SIFT");
    std::vector<KeyPoint> keypoints;
    detector->detect(input, keypoints);

    // Add results to image and save.
    Mat output;
    drawKeypoints(input, keypoints, output);
    imwrite("sift_result.jpg", output);

    return 0;
}